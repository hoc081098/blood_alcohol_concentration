import 'dart:async';

import 'package:disposebag/disposebag.dart';
import 'package:distinct_value_connectable_stream/distinct_value_connectable_stream.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc_pattern/flutter_bloc_pattern.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import 'calculators.dart';
import 'data.dart';
import 'models.dart';

// ignore_for_file: close_sinks

typedef Function0<R> = R Function();
typedef Function1<T, R> = R Function(T);

class Bloc implements BaseBloc {
  final Function1<Gender, void> genderChanged;
  final Function1<int, void> ageChanged;
  final Function1<int, void> weightChanged;
  final Function1<int, void> heightChanged;
  final Function1<DrinkType, void> typeChanged;
  final Function1<Beer, void> beerChanged;
  final Function1<int, void> beerCupChanged;
  final Function1<int, void> minutesChanged;
  final Function1<Alcohol, void> alcoholChanged;
  final Function1<int, void> alcoholCupChanged;
  final Function1<int, void> alcoholPerCupChanged;
  final Function0<void> reset;
  final Function0<void> submit;

  final ValueStream<Gender> gender$;
  final ValueStream<int> age$;
  final ValueStream<int> weight$;
  final ValueStream<int> height$;
  final ValueStream<DrinkType> type$;
  final ValueStream<Beer> beer$;
  final ValueStream<int> beerCup$;
  final ValueStream<int> minutes$;
  final ValueStream<Alcohol> alcohol$;
  final ValueStream<int> alcoholCup$;
  final ValueStream<int> alcoholPerCup$;
  final ValueStream<Outputs> outputs$;
  final Stream<Message> message$;

  final Function0<void> _dispose;

  Bloc._(
    this._dispose, {
    @required this.genderChanged,
    @required this.ageChanged,
    @required this.weightChanged,
    @required this.heightChanged,
    @required this.typeChanged,
    @required this.gender$,
    @required this.age$,
    @required this.weight$,
    @required this.height$,
    @required this.type$,
    @required this.beerChanged,
    @required this.beer$,
    @required this.beerCup$,
    @required this.beerCupChanged,
    @required this.minutes$,
    @required this.minutesChanged,
    @required this.reset,
    @required this.submit,
    @required this.outputs$,
    @required this.message$,
    @required this.alcoholChanged,
    @required this.alcoholCupChanged,
    @required this.alcoholPerCupChanged,
    @required this.alcohol$,
    @required this.alcoholCup$,
    @required this.alcoholPerCup$,
  });

  factory Bloc() {
    final genderS = PublishSubject<Gender>();
    final ageS = PublishSubject<int>();
    final weightS = PublishSubject<int>();
    final heightS = PublishSubject<int>();
    final typeS = PublishSubject<DrinkType>();
    final beerS = PublishSubject<Beer>();
    final beerCupS = PublishSubject<int>();
    final minutesS = PublishSubject<int>();
    final alcoholS = PublishSubject<Alcohol>();
    final alcoholCupS = PublishSubject<int>();
    final alcoholPerCupS = PublishSubject<int>();
    final resetS = PublishSubject<void>();
    final submitS = PublishSubject<void>();

    final inputSubjects = [
      genderS,
      ageS,
      weightS,
      heightS,
      typeS,
      beerS,
      beerCupS,
      minutesS,
      alcoholS,
      alcoholCupS,
      alcoholPerCupS,
    ];

    final controllers = <StreamController>[...inputSubjects, resetS];

    final gender$ = genderS.pVSD(Gender.male);
    final age$ = ageS.pVSD(20);
    final weight$ = weightS.pVSD(50);
    final height$ = heightS.pVSD(170);
    final type$ = typeS.pVSD(DrinkType.beer);
    final beer$ = beerS.pVSD(beers.first);
    final beerCup$ = beerCupS.pVSD(5);
    final minutes$ = minutesS.pVSD(60);
    final alcohol$ = alcoholS.pVSD(alcohols.first);
    final alcoholCup$ = alcoholCupS.pVSD(1);
    final alcoholPerCup$ = alcoholPerCupS.pVSD(50);

    Stream<Inputs> inputs$ = submitS.map(
      (_) {
        switch (type$.value) {
          case DrinkType.beer:
            return BeerInputs(
              gender: gender$.value,
              age: age$.value,
              weight: weight$.value,
              height: height$.value,
              beer: beer$.value,
              beerCup: beerCup$.value,
              minutes: minutes$.value,
            );
          case DrinkType.alcohol:
            return AlcoholInputs(
              gender: gender$.value,
              age: age$.value,
              weight: weight$.value,
              height: height$.value,
              minutes: minutes$.value,
              alcohol: alcohol$.value,
              cup: alcoholCup$.value,
              perCup: alcoholPerCup$.value,
            );
          default:
            return EmptyInputs();
        }
      },
    ).share();

    final outputs$ = Rx.merge([
      inputs$.where((i) => i.isValid()).doOnData(print).map(calculateOutputs),
      resetS.mapTo(null),
    ]).publishValue();

    final message$ = inputs$
        .where((i) => !i.isValid())
        .map((_) => const InvalidInputsMessage())
        .share();

    final subscriptions = <StreamSubscription>[
      resetS.listen((_) => inputSubjects.forEach((s) => s.add(null))),
      // listen
      gender$.listen((g) => print('[BLOC] gender=$g')),
      age$.listen((a) => print('[BLOC] age=$a')),
      weight$.listen((w) => print('[BLOC] weight=$w')),
      height$.listen((h) => print('[BLOC] height=$h')),
      type$.listen((t) => print('[BLOC] type=$t')),
      beer$.listen((b) => print('[BLOC] beer=$b')),
      beerCup$.listen((c) => print('[BLOC] beerCup=$c')),
      minutes$.listen((m) => print('[BLOC] minutes=$m')),
      outputs$.listen((o) => print('[BLOC] outputs=$o')),
      alcohol$.listen((a) => print('[BLOC] alcohol=$a')),
      alcoholCup$.listen((a) => print('[BLOC] alcoholCup=$a')),
      alcoholPerCup$.listen((a) => print('[BLOC] alcoholPerCup=$a')),
      // connect
      gender$.connect(),
      age$.connect(),
      weight$.connect(),
      height$.connect(),
      type$.connect(),
      beer$.connect(),
      beerCup$.connect(),
      minutes$.connect(),
      outputs$.connect(),
      alcohol$.connect(),
      alcoholCup$.connect(),
      alcoholPerCup$.connect(),
    ];

    return Bloc._(
      DisposeBag([...controllers, ...subscriptions]).dispose,
      //
      genderChanged: genderS.add,
      ageChanged: ageS.add,
      weightChanged: weightS.add,
      heightChanged: heightS.add,
      typeChanged: typeS.add,
      beerChanged: beerS.add,
      beerCupChanged: beerCupS.add,
      minutesChanged: minutesS.add,
      reset: () => resetS.add(null),
      submit: () => submitS.add(null),
      alcoholChanged: alcoholS.add,
      alcoholCupChanged: alcoholCupS.add,
      alcoholPerCupChanged: alcoholPerCupS.add,
      //
      gender$: gender$,
      age$: age$,
      weight$: weight$,
      height$: height$,
      type$: type$,
      beer$: beer$,
      beerCup$: beerCup$,
      minutes$: minutes$,
      outputs$: outputs$,
      message$: message$,
      alcohol$: alcohol$,
      alcoholCup$: alcoholCup$,
      alcoholPerCup$: alcoholPerCup$,
    );
  }

  @override
  void dispose() => _dispose();
}

extension PVSD<T> on Stream<T> {
  DistinctValueConnectableStream<T> pVSD(T seeded) =>
      publishValueSeededDistinct(seedValue: seeded);
}
