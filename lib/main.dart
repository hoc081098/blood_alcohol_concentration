import 'dart:async';

import 'package:blood_alcohol_concentration/data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc_pattern/flutter_bloc_pattern.dart';

import 'bloc.dart';
import 'models.dart';
import 'painter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Đo nồng độ cồn',
      theme: ThemeData(
        fontFamily: 'GoogleSans',
        primarySwatch: Colors.purple,
      ),
      home: BlocProvider<Bloc>(
        initBloc: () => Bloc(),
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  StreamSubscription<Message> subscription;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    subscription ??= context.bloc.message$.listen((message) {
      if (message is InvalidInputsMessage) {
        return scaffoldKey.snackBar('Invalid inputs');
      }
    });
  }

  @override
  void dispose() {
    subscription.cancel();
    subscription = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Đo nồng độ cồn'),
      ),
      body: Container(
        color: Colors.white,
        constraints: BoxConstraints.expand(),
        child: CustomPaint(
          painter: MyPainter(),
          child: Center(
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: <Widget>[
                InputCard(),
                InfoCard(),
                NoteCard(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class InfoCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Outputs>(
      stream: context.bloc.outputs$,
      initialData: context.bloc.outputs$.value,
      builder: (context, snapshot) {
        Widget child;

        if (!snapshot.hasData) {
          child = Container(width: 0, height: 0);
        } else {
          final outputs = snapshot.data;

          child = MyCard(
            key: ValueKey(outputs),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  InfoText(
                    title: 'Đã nốc vào: ',
                    content: outputs.ingestedMl,
                  ),
                  InfoText(
                    title: 'Còn lại trong cơ thể: ',
                    content: outputs.remainingMl,
                  ),
                  InfoText(
                    title: 'Nồng độ cồn trong máu: ',
                    content: outputs.bac,
                  ),
                  InfoText(
                    title: 'Thời gian vận công để hết men: ',
                    content: outputs.minutesToOhFive,
                  ),
                  InfoText(
                    title: 'Chỉ định: ',
                    content: outputs.status,
                  ),
                ],
              ),
            ),
          );
        }

        return AnimatedSwitcher(
          duration: const Duration(milliseconds: 400),
          child: child,
          transitionBuilder: (Widget child, Animation<double> animation) {
            return FadeTransition(
              opacity: animation,
              child: ScaleTransition(
                child: child,
                scale: animation,
              ),
            );
          },
        );
      },
    );
  }
}

class InfoText extends StatelessWidget {
  final String title;
  final String content;

  const InfoText({
    Key key,
    @required this.title,
    @required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: title,
        style: Theme.of(context).textTheme.subtitle.copyWith(
              fontWeight: FontWeight.w400,
              fontSize: 15,
              color: Colors.black.withOpacity(0.65),
            ),
        children: [
          TextSpan(
            text: content,
            style: Theme.of(context).textTheme.subtitle.copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
          )
        ],
      ),
    );
  }
}

class NoteCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyCard(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          'Kết quả ước tính nồng độ cồn trong hơi thở không hoàn toàn chính xác vì còn những yếu tố khác ảnh'
          ' hưởng lên kết quả như yếu tố di truyền, tình trạng sức khỏe, thói quen phá mồi hay đi đái của mỗi người...Do'
          ' đó, để đảm bảo an toàn 100% khi tham gia giao thông tốt nhất chúng ta nên THÔNG chốt hoặc KHÔNG nên'
          ' uống.',
          style: Theme.of(context).textTheme.subtitle.copyWith(
                wordSpacing: 2,
                fontSize: 15,
              ),
        ),
      ),
    );
  }
}

class InputCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;

    return MyCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            'Đo nồng độ cồn',
            style: textTheme.title.copyWith(fontSize: 17),
          ),
          const SizedBox(
            height: 16,
          ),
          buildGenderRow(context),
          buildAgeRow(context),
          buildWeightRow(context),
          buildHeightRow(context),
          buildTypeRow(context),
          StreamBuilder<DrinkType>(
            stream: context.bloc.type$,
            initialData: context.bloc.type$.value,
            builder: (context, snapshot) {
              Widget child;
              switch (snapshot.data) {
                case DrinkType.beer:
                  child = Column(
                    key: UniqueKey(),
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      buildBeerRow(context),
                      buildBeerCupRow(context),
                    ],
                  );
                  break;
                case DrinkType.alcohol:
                  child = Column(
                    key: UniqueKey(),
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      buildAlcoholRow(context),
                      buildAlcoholPerCupRow(context),
                      buildAlcoholCupRow(context),
                    ],
                  );
                  break;
                default:
                  child = Container(width: 0, height: 0);
                  break;
              }
              return AnimatedSwitcher(
                duration: const Duration(milliseconds: 500),
                child: child,
                transitionBuilder: (Widget child, Animation<double> animation) {
                  return FadeTransition(
                    opacity: animation,
                    child: ScaleTransition(
                      child: child,
                      scale: animation,
                    ),
                  );
                },
              );
            },
          ),
          buildMinutesCupRow(context),
          buildButtons(context),
        ],
      ),
    );
  }

  Widget buildTypeRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Đồ uống'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<DrinkType>(
            stream: context.bloc.type$,
            initialData: context.bloc.type$.value,
            builder: (context, snapshot) {
              return DropdownButton<DrinkType>(
                items: [
                  DropdownMenuItem(
                    child: Text('Bia'),
                    value: DrinkType.beer,
                  ),
                  DropdownMenuItem(
                    child: Text('Rượu'),
                    value: DrinkType.alcohol,
                  ),
                ],
                onChanged: context.bloc.typeChanged,
                value: snapshot.data,
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buildHeightRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Chiều cao (cm)'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<int>(
            stream: context.bloc.height$,
            initialData: context.bloc.height$.value,
            builder: (context, snapshot) {
              return DropdownButton<int>(
                items: [
                  for (int i = 1; i <= 200; i++)
                    DropdownMenuItem(
                      child: Text(i.toString()),
                      value: i,
                    ),
                ],
                onChanged: context.bloc.heightChanged,
                value: snapshot.data,
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buildWeightRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Cân nặng (kg)'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<int>(
              stream: context.bloc.weight$,
              initialData: context.bloc.weight$.value,
              builder: (context, snapshot) {
                return DropdownButton<int>(
                  items: [
                    for (int i = 1; i <= 200; i++)
                      DropdownMenuItem(
                        child: Text(i.toString()),
                        value: i,
                      ),
                  ],
                  onChanged: context.bloc.weightChanged,
                  value: snapshot.data,
                );
              }),
        ),
      ],
    );
  }

  Widget buildAgeRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Tuổi'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<int>(
            stream: context.bloc.age$,
            initialData: context.bloc.age$.value,
            builder: (context, snapshot) {
              return DropdownButton<int>(
                items: [
                  for (int i = 1; i <= 100; i++)
                    DropdownMenuItem(
                      child: Text(i.toString()),
                      value: i,
                    ),
                ],
                onChanged: context.bloc.ageChanged,
                value: snapshot.data,
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buildGenderRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Giới tính'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<Gender>(
            stream: context.bloc.gender$,
            initialData: context.bloc.gender$.value,
            builder: (context, snapshot) {
              return DropdownButton<Gender>(
                items: [
                  DropdownMenuItem(
                    child: Text('Nam'),
                    value: Gender.male,
                  ),
                  DropdownMenuItem(
                    child: Text('Nữ'),
                    value: Gender.female,
                  ),
                ],
                onChanged: context.bloc.genderChanged,
                value: snapshot.data,
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buildBeerRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Hãng bia'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<Beer>(
            stream: context.bloc.beer$,
            initialData: context.bloc.beer$.value,
            builder: (context, snapshot) {
              return DropdownButton<Beer>(
                items: [
                  for (final b in beers)
                    DropdownMenuItem(
                      child: Text(b.name),
                      value: b,
                    ),
                ],
                onChanged: context.bloc.beerChanged,
                value: snapshot.data,
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buildBeerCupRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Tửu lượng'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<int>(
            stream: context.bloc.beerCup$,
            initialData: context.bloc.beerCup$.value,
            builder: (context, snapshot) {
              return DropdownButton<int>(
                items: [
                  for (int i = 1; i <= 30; i++)
                    DropdownMenuItem(
                      child: Text('$i lon'),
                      value: i,
                    ),
                ],
                onChanged: context.bloc.beerCupChanged,
                value: snapshot.data,
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buildMinutesCupRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Thời gian (phút)'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<int>(
            stream: context.bloc.minutes$,
            initialData: context.bloc.minutes$.value,
            builder: (context, snapshot) {
              return DropdownButton<int>(
                items: [
                  for (int i = 1; i <= 10 * 60; i++)
                    DropdownMenuItem(
                      child: Text(i.toString()),
                      value: i,
                    ),
                ],
                onChanged: context.bloc.minutesChanged,
                value: snapshot.data,
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buildButtons(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        RaisedButton(
          onPressed: context.bloc.submit,
          child: Text(
            'Đo',
            style: Theme.of(context).textTheme.button.copyWith(
                  color: Colors.white,
                ),
          ),
          color: Theme.of(context).accentColor,
          padding: const EdgeInsets.all(12),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
        ),
        const SizedBox(width: 16),
        RaisedButton(
          onPressed: context.bloc.reset,
          child: Text(
            'Reset',
            style: Theme.of(context).textTheme.button.copyWith(
                  color: Colors.white,
                ),
          ),
          color: Theme.of(context).accentColor,
          padding: const EdgeInsets.all(12),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
        ),
      ],
    );
  }

  Widget buildAlcoholRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Loại rượu'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<Alcohol>(
            stream: context.bloc.alcohol$,
            initialData: context.bloc.alcohol$.value,
            builder: (context, snapshot) {
              return DropdownButton<Alcohol>(
                items: [
                  for (final b in alcohols)
                    DropdownMenuItem(
                      child: Text(b.name),
                      value: b,
                    ),
                ],
                onChanged: context.bloc.alcoholChanged,
                value: snapshot.data,
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buildAlcoholCupRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Tửu lượng'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<int>(
            stream: context.bloc.alcoholCup$,
            initialData: context.bloc.alcoholCup$.value,
            builder: (context, snapshot) {
              return DropdownButton<int>(
                items: [
                  for (int i = 1; i <= 50; i++)
                    DropdownMenuItem(
                      child: Text('$i ly'),
                      value: i,
                    ),
                ],
                onChanged: context.bloc.alcoholCupChanged,
                value: snapshot.data,
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buildAlcoholPerCupRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('Loại ly'),
        const SizedBox(
          width: 24,
        ),
        Expanded(
          child: StreamBuilder<int>(
            stream: context.bloc.alcoholPerCup$,
            initialData: context.bloc.alcoholPerCup$.value,
            builder: (context, snapshot) {
              return DropdownButton<int>(
                items: [
                  for (final i in const [50, 100, 200, 300, 400, 500])
                    DropdownMenuItem(
                      child: Text('$i ml'),
                      value: i,
                    ),
                ],
                onChanged: context.bloc.alcoholPerCupChanged,
                value: snapshot.data,
              );
            },
          ),
        ),
      ],
    );
  }
}

class MyCard extends StatelessWidget {
  final Widget child;

  const MyCard({Key key, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 6,
        left: 12,
        right: 12,
        bottom: 6,
      ),
      child: Card(
        elevation: 12,
        clipBehavior: Clip.antiAlias,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: child,
        ),
      ),
    );
  }
}

extension on BuildContext {
  Bloc get bloc => BlocProvider.of<Bloc>(this);
}

extension on GlobalKey<ScaffoldState> {
  void snackBar(String message) {
    currentState?.showSnackBar(
      SnackBar(
        content: Text(message),
        duration: const Duration(
          seconds: 1,
          milliseconds: 500,
        ),
      ),
    );
  }
}
