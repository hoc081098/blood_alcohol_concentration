import 'models.dart';

Outputs calculateOutputs(Inputs inputs) {
  final double ingestedMl = _alcoholMlInDrinks(inputs);

  if (ingestedMl.compareTo(0) == 0) {
    return Outputs(
      '0 ml',
      '0 ml',
      '0 %',
      '0',
      '',
    );
  }

  double elapsedTime = inputs.minutes / 60;
  double remainingMl = _rupCalcRemaining(ingestedMl, elapsedTime);
  if (remainingMl < 0) {
    remainingMl = 0;
  }

  double bodyWaterMl = _rupCalcBodyWater(
    inputs.height,
    inputs.weight,
    inputs.age,
    inputs.gender,
  );
  double bac = _rupCalcBac(remainingMl, bodyWaterMl);
  int minutesToOhFive = _rupCalcMinutesFromBac(bac, bodyWaterMl);
  final list = _getStatus(bac);

  return Outputs(
    '${ingestedMl.toStringAsFixed(2)} ml',
    '${remainingMl.toStringAsFixed(2)} ml',
    '${bac.toStringAsFixed(3)} %',
    '$minutesToOhFive phút',
    list[0],
  );
}

double _alcoholMlInDrinks(Inputs inputs) {
  if (inputs is BeerInputs) {
    return inputs.beer.volume * inputs.beer.level * inputs.beerCup * 0.01;
  }
  if (inputs is AlcoholInputs) {
    return inputs.cup * inputs.perCup * inputs.alcohol.level * 0.01;
  }
  throw 'Not support';
}

List<String> _getStatus(double outputBAC) {
  var status =
      'Thanh xuân như một chén trà, Tết mà không rượu hết bà thanh xuân.';
  var icon = 'hairy';
  if (outputBAC > 0.35) {
    status = 'Thành thật xin lỗi, bác sĩ chúng tôi đã cố gắng hết sức.';
    icon = 'cuddly';
  } else if (outputBAC > 0.26) {
    status = 'Đảm bảo ngày mai là bạn đếu còn nhớ gì cả.';
    icon = 'cuddly';
  } else if (outputBAC > 0.19) {
    status = 'Đường lên tiên cảnh là đây, đường này là đường của bố.';
    icon = 'devil';
  } else if (outputBAC > 0.12) {
    status = 'Alo Huệ ơi, Huệ ơi Huệ...';
    icon = 'devil';
  } else if (outputBAC > 0.07) {
    status = 'Các hãng xe công nghệ đang chờ bạn.';
    icon = 'gummy';
  } else if (outputBAC > 0.05) {
    status = 'Bắt đầu thông chốt được rồi đấy.';
    icon = 'gummy';
  } else if (outputBAC > 0.035) {
    status = 'Chén chú chén anh, rượu vơi lại đầy, vô tư đê.';
    icon = 'hairy';
  } else if (outputBAC > 0.005) {
    status = 'Anh CSGT ơi, đố anh bắt được em đó, hí hí.';
    icon = 'hairy';
  }
  return [status, icon];
}

double _rupCalcRemaining(double ingested, double elapsedTimeHours) {
  return (ingested - (_METABOLIC_REMOVAL_RATE_MLPH * elapsedTimeHours));
}

// Result in % g/ml
double _rupCalcBac(double alcoholMl, double bodyWaterMl) {
  double bloodMl = bodyWaterMl / _WATER_CONTENT_OF_BLOOD;
  double alcoholGrams = alcoholMl * _ALCOHOL_SPECIFIC_GRAVITY;
  double bac = 100 * (alcoholGrams / bloodMl);
  return bac;
}

double _rupCalcBodyWater(int height, int weight, int age, Gender sex) {
  double HEIGHT_FACTOR = (sex == Gender.male) ? 0.1074 : 0.1069;
  double WEIGHT_FACTOR = (sex == Gender.male) ? 0.3362 : 0.2466;
  num AGE_FACTOR = (sex == Gender.male) ? 0.09516 : 0;
  double BODY_WATER_CONST = (sex == Gender.male) ? 2.447 : 2.097;
  double h = HEIGHT_FACTOR * height;
  double w = WEIGHT_FACTOR * weight;
  num a = AGE_FACTOR * age;
  double ml = (h - a + w + BODY_WATER_CONST) * 1000;
  return ml;
}

// BAC in % g/ml
int _rupCalcMinutesFromBac(double bac, double bodyWaterMl) {
  if (bac <= 0) return 0;
  final alcoholMl = _rupCalcAlcoholRemainingFromBAC(bac, bodyWaterMl);
  return (alcoholMl * 60 / _METABOLIC_REMOVAL_RATE_MLPH).ceil();
}

// BAC in % g/ml
double _rupCalcAlcoholRemainingFromBAC(double bac, double bodyWaterMl) {
  final bloodMl = bodyWaterMl / _WATER_CONTENT_OF_BLOOD;
  final alcoholGrams = bloodMl * bac / 100;
  final alcoholMl = alcoholGrams / _ALCOHOL_SPECIFIC_GRAVITY;
  return alcoholMl;
}

const double _METABOLIC_REMOVAL_RATE_GPH = 7.9; // in g/hr
const double _WATER_CONTENT_OF_BLOOD = 0.8157;
const double _ALCOHOL_SPECIFIC_GRAVITY = 0.79;

const double _METABOLIC_REMOVAL_RATE_MLPH =
    _METABOLIC_REMOVAL_RATE_GPH / _ALCOHOL_SPECIFIC_GRAVITY;
