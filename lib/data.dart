import 'models.dart';

const _beers = [
  {"volume": 330, "level": 5.3, "name": "333"},
  {"volume": 250, "level": 5, "name": "Heineken 250ml"},
  {"volume": 330, "level": 5, "name": "Heineken 330ml"},
  {"volume": 500, "level": 7, "name": "Heineken 500ml"},
  {"volume": 330, "level": 5, "name": "Tiger 330ml"},
  {"volume": 500, "level": 5, "name": "Tiger 500ml"},
  {"volume": 330, "level": 5, "name": "Carlberg 330ml"},
  {"volume": 500, "level": 5, "name": "Carlberg 500ml"},
  {"volume": 330, "level": 4.6, "name": "Hà Nội 330ml (4.6%)"},
  {"volume": 330, "level": 5.1, "name": "Hà Nội 330ml (5.1%)"},
  {"volume": 450, "level": 4.2, "name": "Hà Nội 450ml"},
  {"volume": 330, "level": 4.7, "name": "Huda 330ml"},
  {"volume": 350, "level": 4.7, "name": "Huda 350ml"},
  {"volume": 500, "level": 5, "name": "Huda 500ml"},
  {"volume": 330, "level": 4, "name": "Larue 330ml"},
  {"volume": 355, "level": 4, "name": "Larue 355ml"},
  {"volume": 355, "level": 4.9, "name": "Sài gòn Export"},
  {"volume": 450, "level": 4.3, "name": "Sài gòn Lager"},
  {"volume": 330, "level": 4.9, "name": "Sàn gòn Special"},
  {"volume": 330, "level": 5, "name": "Sam Miguel 330ml"},
  {"volume": 500, "level": 5, "name": "Sam Miguel 500ml"},
  {"volume": 330, "level": 5, "name": "Sapporo 330ml"},
  {"volume": 650, "level": 5.2, "name": "Sapporo 650ml"}
];

final List<Beer> beers =
    _beers.map((json) => Beer.fromJson(json)).toList(growable: false);

const _alcohols = [
  {"name": "Gạo", "level": "40"},
  {"name": "Nếp", "level": "40"},
  {"name": "Camus", "level": "40"},
  {"name": "Hines", "level": "40"},
  {"name": "Vodka", "level": "40"},
  {"name": "Bordeaux", "level": "12"},
  {"name": "Bourgogne", "level": "12"},
  {"name": "Champagne", "level": "12"},
  {"name": "Côte du Rhône", "level": "12"},
  {"name": "Chardonnay", "level": "15"},
  {"name": "Cabernet Sauvignon", "level": "15"},
  {"name": "Merlot", "level": "15"},
  {"name": "Hennessy", "level": "40"},
  {"name": "Remy Martin", "level": "40"},
  {"name": "Martel, Otard", "level": "40"},
  {"name": "Courvoisier", "level": "40"}
];

final List<Alcohol> alcohols =
    _alcohols.map((json) => Alcohol.fromJson(json)).toList(growable: false);
