import 'package:meta/meta.dart';

///

enum Gender { male, female }
enum DrinkType { beer, alcohol }

class Beer {
  final double volume;
  final double level;
  final String name;

  const Beer(this.volume, this.level, this.name);

  factory Beer.fromJson(Map<String, Object> json) => Beer(
        (json['volume'] as num).toDouble(),
        (json['level'] as num).toDouble(),
        json['name'],
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Beer &&
          runtimeType == other.runtimeType &&
          volume == other.volume &&
          level == other.level &&
          name == other.name;

  @override
  int get hashCode => volume.hashCode ^ level.hashCode ^ name.hashCode;

  @override
  String toString() {
    return 'Beer{volume: $volume, level: $level, name: $name}';
  }
}

class Alcohol {
  final double level;
  final String name;

  const Alcohol(this.level, this.name);

  factory Alcohol.fromJson(Map<String, dynamic> json) {
    return Alcohol(
      double.parse(json['level']),
      json['name'],
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Alcohol &&
          runtimeType == other.runtimeType &&
          level == other.level &&
          name == other.name;

  @override
  int get hashCode => level.hashCode ^ name.hashCode;

  @override
  String toString() {
    return 'Alcohol{level: $level, name: $name}';
  }
}

/// Inputs

abstract class Inputs {
  Gender get gender;

  int get age;

  int get weight;

  int get height;

  int get minutes;

  bool _isValidCached;

  bool isValid() => _isValidCached ??=
      [gender, age, weight, height, minutes].every((e) => e != null) &&
          _isValid() &&
          [age, weight, height, minutes].every((e) => e > 0);

  bool _isValid();
}

class BeerInputs extends Inputs {
  @override
  final Gender gender;

  @override
  final int age;

  @override
  final int weight;

  @override
  final int height;

  @override
  final int minutes;

  final Beer beer;
  final int beerCup;

  BeerInputs({
    @required this.gender,
    @required this.age,
    @required this.weight,
    @required this.height,
    @required this.beer,
    @required this.beerCup,
    @required this.minutes,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BeerInputs &&
          runtimeType == other.runtimeType &&
          gender == other.gender &&
          age == other.age &&
          weight == other.weight &&
          height == other.height &&
          beer == other.beer &&
          beerCup == other.beerCup &&
          minutes == other.minutes;

  @override
  int get hashCode =>
      gender.hashCode ^
      age.hashCode ^
      weight.hashCode ^
      height.hashCode ^
      beer.hashCode ^
      beerCup.hashCode ^
      minutes.hashCode;

  @override
  String toString() {
    return 'InputsBeer{gender: $gender, age: $age, weight: $weight, height: $height,'
        ' beer: $beer, beerCup: $beerCup, minutes: $minutes}';
  }

  @override
  bool _isValid() => beer != null && beerCup != null && beerCup > 0;
}

class AlcoholInputs extends Inputs {
  @override
  final Gender gender;

  @override
  final int age;

  @override
  final int weight;

  @override
  final int height;

  @override
  final int minutes;

  final Alcohol alcohol;
  final int perCup;
  final int cup;

  @override
  bool _isValid() =>
      [alcohol, perCup, cup].every((e) => e != null) && perCup > 0 && cup > 0;

  AlcoholInputs({
    @required this.gender,
    @required this.age,
    @required this.weight,
    @required this.height,
    @required this.minutes,
    @required this.alcohol,
    @required this.cup,
    @required this.perCup,
  });
}

class EmptyInputs extends Inputs {
  @override
  int get age => null;

  @override
  Gender get gender => null;

  @override
  int get height => null;

  @override
  int get minutes => null;

  @override
  int get weight => null;

  @override
  bool _isValid() => false;
}

/// Outputs
class Outputs {
  final String ingestedMl;
  final String remainingMl;
  final String bac;
  final String minutesToOhFive;
  final String status;

  const Outputs(
    this.ingestedMl,
    this.remainingMl,
    this.bac,
    this.minutesToOhFive,
    this.status,
  );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Outputs &&
          runtimeType == other.runtimeType &&
          ingestedMl == other.ingestedMl &&
          remainingMl == other.remainingMl &&
          bac == other.bac &&
          minutesToOhFive == other.minutesToOhFive &&
          status == other.status;

  @override
  int get hashCode =>
      ingestedMl.hashCode ^
      remainingMl.hashCode ^
      bac.hashCode ^
      minutesToOhFive.hashCode ^
      status.hashCode;

  @override
  String toString() {
    return 'Outputs{ingestedMl: $ingestedMl, remainingMl: $remainingMl, bac: $bac,'
        ' minutesToOhFive: $minutesToOhFive, status: $status}';
  }
}

/// Message

abstract class Message {}

class InvalidInputsMessage implements Message {
  const InvalidInputsMessage();
}
