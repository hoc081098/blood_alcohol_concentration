import 'dart:math';

import 'package:flutter/material.dart';

class MyPainter extends CustomPainter {
  List<_Circle> circles;

  @override
  void paint(Canvas canvas, Size size) {
    if (circles == null) {
      final circles = <_Circle>[];
      final random = Random();

      _getNext() {
        final x = random.nextDouble() * size.width;
        final y = random.nextDouble() * size.height;
        final radius = random.nextDouble() * 0.3 * size.width;
        if (radius < 0.05 * size.width) {
          return null;
        }

        final any = circles.any((c) {
          final distance = _distance(c.x, c.y, x, y);
          return distance <= (c.radius + radius);
        });
        if (any) {
          return null;
        }
        return _Circle(x, y, radius);
      }

      for (int i = 0; i < 300; i++) {
        var getNext = _getNext();
        if (getNext != null) {
          circles.add(getNext);
          if (circles.length == 5) {
            break;
          }
        }
      }
      this.circles = circles;
    }

    final paint = Paint()
      ..color = Colors.purpleAccent
      ..style = PaintingStyle.fill;

    for (final c in circles) {
      canvas.drawCircle(
        Offset(c.x, c.y),
        c.radius,
        paint,
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class _Circle {
  final double x;
  final double y;
  final double radius;

  const _Circle(this.x, this.y, this.radius);
}

double _distance(double x1, double y1, double x2, double y2) =>
    sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
